const btnEl = document.querySelector("#tinh-ngay");

function tinhNgay() {
  const thang = document.getElementById("thang").value * 1;
  const nam = document.getElementById("nam").value * 1;
  const message = document.querySelector(".message");

  function switchMonth() {
    switch (thang) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
          message.innerText = `tháng ${thang} năm ${nam} có 31 ngày`;
          break;
        case 4:
        case 6:
        case 9:
        case 11:
          message.innerText = `tháng ${thang} năm ${nam} có 30 ngày`;
          break;
    }
  }

  if (nam >= 1920) {
    if ((nam % 4 === 0 && nam % 100 !== 0) || nam % 400 === 0) {
        switchMonth()
        if(thang === 2) {
            message.innerText = `tháng ${thang} năm ${nam} có 29 ngày`;
        }
    } else {
      switchMonth()
      if(thang === 2) {
        message.innerText = `tháng ${thang} năm ${nam} có 28 ngày`;
      }
    }
  }else {
    alert("vui lòng nhập năm >= 1920")
  }
}

btnEl.addEventListener("click", function () {
  tinhNgay();
});
