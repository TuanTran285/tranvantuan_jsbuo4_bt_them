const btn = document.querySelector("#btn");
function docSo() {
  let input = document.getElementById("nhap-so").value * 1;
  let message = document.querySelector(".message");
  function print(text) {
    message.innerText = text;
  }
  if (input >= 100 && input <= 999) {
    const donVi = input % 10;
    const chuc = Math.floor((input / 10) % 10);
    const tram = Math.floor(input / 100);
    let a;
    let b;
    let c;
    switch (tram) {
      case 1:
        a = "Một trăm";
        break;
      case 2:
        a = "Hai trăm";
        break;
      case 3:
        a = "Ba trăm";
        break;
      case 4:
        a = "Bốn trăm";
        break;
      case 5:
        a = "Năm trăm";
        break;
      case 6:
        a = "Sáu trăm";
        break;
      case 7:
        a = "Bảy trăm";
        break;
      case 8:
        a = "Tám trăm";
        break;
      case 9:
        a = "Chín trăm";
        break;
    }
    if (chuc % 10 == 0 && tram != 0) {
      b = "lẻ";
    }
    switch (chuc) {
      case 1:
        b = "mười";
        break;
      case 2:
        b = "hai mươi";
        break;
      case 3:
        b = "ba mươi";
        break;
      case 4:
        b = "bốn mươi";
        break;
      case 5:
        b = "năm mươi";

        break;
      case 6:
        b = "sáu mươi";
        break;
      case 7:
        b = "bảy mươi";
        break;
      case 8:
        b = "tám mươi";
        break;
      case 9:
        b = "chin mươi";
        break;
    }
    switch (donVi) {
      case 1:
        c = "một";
        break;
      case 2:
        c = "hai";
        break;
      case 3:
        c = "ba";
        break;
      case 4:
        c = "bốn";
        break;
      case 5:
        c = "năm";
        break;
      case 6:
        c = "sáu";
        break;
      case 7:
        c = "bảy";
        break;
      case 8:
        c = "tám";
        break;
      case 9:
        c = "chín";
        break;
    }
    print(`${a} ${b} ${c}`);
  } else {
    alert("Dữ liệu không hợp lệ");
  }
}

btn.addEventListener("click", function (e) {
  docSo();
});
