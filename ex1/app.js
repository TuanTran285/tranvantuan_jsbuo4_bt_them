const prevEl = document.querySelector("#ngay-hom-qua");
const nextEl = document.querySelector("#ngay-mai");

// cái này e tự code nên hơi rối ạ


// click ngày mai
function ngayMai() {
  let ngayEl = document.querySelector("#ngay").value * 1;
  let thangEl = document.querySelector("#thang").value * 1;
  let namEl = document.querySelector("#nam").value * 1;
  let message = document.querySelector(".message");
  function innerTime() {
    message.innerText = `${ngayEl}/${thangEl}/${namEl}`;
    message.style.display = "flex";
  }
  function errorTime() {
    alert("Dữ liệu không hợp lệ");
    message.style.display = "none";
  }

  if (namEl >= 1920) {
    if ((namEl % 4 == 0 && namEl % 100 !== 0) || namEl % 400 == 0) {
      switch (thangEl) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
          if (ngayEl > 0 && ngayEl < 31) {
            ngayEl++;
            innerTime();
          } else if (thangEl === 12 && ngayEl === 31) {
            ngayEl = 1;
            thangEl = 1;
            namEl++;
            innerTime();
          } else if (ngayEl === 31) {
            ngayEl = 1;
            thangEl++;
            innerTime();
          } else {
            errorTime();
          }
          break;
        case 4:
        case 6:
        case 9:
        case 11:
          if (ngayEl > 0 && ngayEl < 30) {
            ngayEl++;
            innerTime();
          } else if (ngayEl === 30) {
            ngayEl = 1;
            thangEl++;
            innerTime();
          } else {
            errorTime();
          }
          break;
      }
      if (thangEl === 2) {
        if (ngayEl > 0 && ngayEl < 29) {
          ngayEl++;
          innerTime();
        } else if (ngayEl === 29) {
          ngayEl = 1;
          thangEl++;
          innerTime();
        } else {
          errorTime();
        }
      }
    } else {
      switch (thangEl) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
          if (ngayEl > 0 && ngayEl < 31) {
            ngayEl++;
            innerTime();
          } else if (thangEl === 12 && ngayEl === 31) {
            ngayEl = 1;
            thangEl = 1;
            namEl++;
            innerTime();
          } else if (ngayEl === 31) {
            ngayEl = 1;
            thangEl++;
            innerTime();
          } else {
            errorTime();
          }
          break;
        case 4:
        case 6:
        case 9:
        case 11:
          if (ngayEl > 0 && ngayEl < 30) {
            ngayEl++;
            innerTime();
          } else if (ngayEl === 30) {
            ngayEl = 1;
            thangEl++;
            innerTime();
          } else {
            errorTime();
          }
          break;
        case 2:
          if (ngayEl > 0 && ngayEl < 28) {
            ngayEl++;
            innerTime();
          } else if (ngayEl == 28) {
            ngayEl = 1;
            thangEl++;
            innerTime();
          } else {
            errorTime();
          }
          break;
      }
    }
  } else {
    alert("Năm cần lớn hơn 1920");
  }
}



// click ngày hôm qua
function ngayHomQua() {
  let ngayEl = document.querySelector("#ngay").value * 1;
  let thangEl = document.querySelector("#thang").value * 1;
  let namEl = document.querySelector("#nam").value * 1;
  let message = document.querySelector(".message");
  function innerTime() {
    message.innerText = `${ngayEl}/${thangEl}/${namEl}`;
    message.style.display = "flex";
  }
  function errorTime() {
    alert("Dữ liệu không hợp lệ");
    message.style.display = "none";
  }

  if (namEl >= 1920) {
    if ((namEl % 4 == 0 && namEl % 100 !== 0) || namEl % 400 == 0) {
      switch (thangEl) {
        case 5:
        case 7:
        case 10:
        case 12:
          if (ngayEl === 1) {
            // gán bằng 30
            ngayEl = 30;
            thangEl--;
            innerTime();
          } else if (ngayEl > 0 && ngayEl <= 31) {
            ngayEl--;
            innerTime();
          } else {
            errorTime();
          }
          break;
        case 1:
        case 2:
        case 4:
        case 6:
        case 8:
        case 9:
        case 11:
          if (ngayEl == 1 && thangEl == 1) {
            ngayEl = 31;
            thangEl = 12;
            namEl--;
            innerTime();
          } else if (ngayEl === 1) {
            // gắn bằng 31
            ngayEl = 31;
            thangEl--;
            innerTime();
          } else if (ngayEl > 0 && ngayEl <= 30) {
            ngayEl--;
            innerTime();
          } else {
            errorTime();
          }
          break;
      }
      if (thangEl === 3) {
        if (ngayEl === 1) {
          ngayEl = 29;
          thangEl--;
          innerTime();
        } else if (ngayEl > 0 && ngayEl <= 29) {
          ngayEl--;
          innerTime();
        } else {
          errorTime();
        }
      }
    } else {
      switch (thangEl) {
        case 5:
        case 7:
        case 10:
        case 12:
          if (ngayEl === 1) {
            // gán bằng 30
            ngayEl = 30;
            thangEl--;
            innerTime();
          } else if (ngayEl > 0 && ngayEl <= 31) {
            ngayEl--;
            innerTime();
          } else {
            errorTime();
          }
          break;
        case 1:
        case 2:
        case 4:
        case 6:
        case 8:
        case 9:
        case 11:
          if (ngayEl == 1 && thangEl == 1) {
            ngayEl = 31;
            thangEl = 12;
            namEl--;
            innerTime();
          } else if (ngayEl === 1) {
            // gắn bằng 31
            ngayEl = 31;
            thangEl--;
            innerTime();
          } else if (ngayEl > 0 && ngayEl <= 31) {
            ngayEl--;
            innerTime();
          } else {
            errorTime();
          }
          break;
        case 3:
          if (ngayEl === 1) {
            // găn bằng 28
            ngayEl = 28;
            thangEl--;
            innerTime();
          } else if (ngayEl > 0 && ngayEl <= 30) {
            ngayEl--;
            innerTime();
          } else {
            errorTime();
          }
          break;
      }
    }
  } else {
    alert("Năm cần lớn hơn 1920");
  }
}

prevEl.addEventListener("click", function (e) {
  ngayHomQua();
});

nextEl.addEventListener("click", function (e) {
  ngayMai();
});
