const btnsearch = document.querySelector("#btn-search");

function tinhDoDai() {
  const tenSv1 = document.querySelector("#ten-sv1").value;
  const xSv1 = document.querySelector("#x-sv1").value * 1;
  const ySv1 = document.querySelector("#y-sv1").value * 1;
  const tenSv2 = document.querySelector("#ten-sv2").value;
  const xSv2 = document.querySelector("#x-sv2").value * 1;
  const ySv2 = document.querySelector("#y-sv2").value * 1;
  const tenSv3 = document.querySelector("#ten-sv3").value;
  const xSv3 = document.querySelector("#x-sv3").value * 1;
  const ySv3 = document.querySelector("#y-sv3").value * 1;
  const xTruong = document.querySelector("#x-truong").value * 1;
  const yTruong = document.querySelector("#y-truong").value * 1;

  const formControls = document.querySelectorAll(".form-control");

  formControls.forEach((item) => {
    if (item.value == "") {
      confirm("Vui lòng nhập dữ liệu");
      return;
    }
    const text = document.querySelector("#text");
    const kcSv1 = tinhKC(xSv1, ySv1, xTruong, yTruong);
    const kcSv2 = tinhKC(xSv2, ySv2, xTruong, yTruong);
    const kcSv3 = tinhKC(xSv3, ySv3, xTruong, yTruong);

    let svXaNhat;
    if (kcSv1 > kcSv2 && kcSv1 > kcSv3) {
      svXaNhat = tenSv1;
    } else if (kcSv2 > kcSv1 && kcSv2 > kcSv3) {
      svXaNhat = tenSv2;
    } else {
      svXaNhat = tenSv3;
    }

    text.innerHTML = `Sinh viên xa trường nhất là: ${svXaNhat}`;
  });
}

function tinhKC(xSv, ySv, xTruong, yTruong) {
  return Math.sqrt((xTruong - xSv) * (xTruong - xSv) + (yTruong - ySv) * (yTruong - ySv));
}

btnsearch.addEventListener("click", function () {
  tinhDoDai();
});
